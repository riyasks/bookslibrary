import { observable, action } from 'mobx'
import { Book } from '../models/book';
import { AppEnum } from '../enums/app.enums';



export class BookStore {
  @observable bookList: Book[] = [];
  @observable filterBookList: Book[] = []
  @observable userCart: Book[] = [];


  constructor() {
    this.bookList = require("../data/bookslibrary.books.json");
  }

  @action
  async addBookToCart(_book: Book) {
    if (this.userCart.length < AppEnum.bookLimit) {
      var userAlreadySelected = this.userCart.findIndex((book) => { return book._id === _book._id; })
      if (userAlreadySelected == -1) {
        this.userCart.push(_book);
        var index = this.bookList.findIndex((book) => { return book._id === _book._id; })
        var count = this.bookList[index].availableCount;
        if (count > 0) {
          this.bookList[index].availableCount = this.bookList[index].availableCount - 1;
        }
        //In real scenario of considering the database system such as mongodb we need to update the following things
        // 1,update user chart in user table if needed to optimize the bookslist table query
        // 2,update borrowedUsers array in bookslist table.
        // 3,update book availability count increment in booklist table
      }
      else {
        alert("The book '" + _book.title + "' is already in your borrowed list");
      }
    }
    else {
      alert(AppEnum.bookLimitMessage);
    }
  };

  @action
  async removeBookFromCart(_book: Book) {
    var cartIndex = this.userCart.findIndex((book) => { return book._id === _book._id; })
    this.userCart.splice(cartIndex, 1);
    var bookIndex = this.bookList.findIndex((book) => { return book._id === _book._id; });
    this.bookList[bookIndex].availableCount = this.bookList[bookIndex].availableCount + 1;
    document.getElementById("BorrowedBooks").style.display = this.userCart.length === 0 ? "none" : "inherit";

    //In real scenario of considering the database system such as mongodb we need to update the following things
    // 1,update user chart in user table if needed to optimize the bookslist table query
    // 2,update borrowedUsers array in bookslist table.
    // 3,update book availability count decrement in booklist table
  };


  @action
  getAllBooks() {
    //Fill books in the booklist array and make a deep copy of array in the filtered list
    this.bookList = require("../data/bookslibrary.books.json"); //result
    this.filterBookList = this.bookList;
  }

  @action
  setFilteredBooks(books: Book[]) {
    //Fill the books according to the filter constrains
    this.filterBookList = books;
  }
}

export const bookStore = new BookStore()
