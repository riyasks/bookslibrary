import React from "react";
import { shallow } from "enzyme";
import { App } from "./app";
import { BooksList } from "./components/bookList/books.list";
import Book from "./components/books/book";
import BorrowedListComponent from "./components/borrowedList/borrowed.list";
import AppHeader from "./components/header/header";
import NoBook from "./components/noBooksFound/nobooksfound";
var appWrapper;


describe("App", () => {
    beforeAll(() => {
        appWrapper = shallow(<App />)
    });
    it("App renders without crashing", () => {
        appWrapper = shallow(<App />)
    });
    it("App contains books list component", () => {
        appWrapper.find(BooksList)
    });
    it("App contains book component", () => {
        appWrapper.find(Book)
    });
    it("App contains borrowed list component", () => {
        appWrapper.find(BorrowedListComponent)
    });
    it("App contains borrowed list component", () => {
        appWrapper.find(NoBook)
    });
    it("App contains header component", () => {
        appWrapper.find(AppHeader)
    });

});