//Interface to inject the mobx store of books in all components
import { BookStore } from "../store/books.store";
export interface BookProps {
  bookStore?: BookStore
}

