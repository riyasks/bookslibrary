export class Book {
    _id: string;
    title: string
    isbn: string;
    pageCount: number;
    thumbnailUrl: string;
    status: string;
    authors: string[];
    categories: string[];
    publishedDate: string;
    shortDescription: string;
    longDescription: string;
    borrowedUsers: string[];
    availableCount: number;
}