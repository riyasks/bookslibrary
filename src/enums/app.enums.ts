export enum AppEnum {
    defaultThumb = "https://islandpress.org/sites/default/files/default_book_cover_2015.jpg",//Temperory image for thumb we can make this from the local folder
    bookLimit=2,
    bookNotAvailable="This book is currently not available!",
    noBooksFound="Sorry no books found!",
    noDescriptionFound="No description found!",
    appHeading="Book Lists",
    cartName="Borrowed books",
    bookLimitMessage="Only 2 books allowed!. check the borrowed list"
   
}