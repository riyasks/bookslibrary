import * as React from "react"
import { AppEnum } from "../../enums/app.enums"

const NoBook: React.SFC = () => {
    return (
        <div id="main">
            <div className="fof">
                <h1>{AppEnum.noBooksFound}</h1>
            </div>
        </div>
    )
}

export default NoBook