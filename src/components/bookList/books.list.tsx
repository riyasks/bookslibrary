import React, { Fragment } from 'react';
import { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { BookProps } from '../../models/props';
import { Book } from '../../models/book';
import BookComponent from '../books/book';
import NoBook from '../noBooksFound/nobooksfound'
import { AppEnum } from '../../enums/app.enums';
@inject('bookStore')
@observer
export class BooksList extends Component<BookProps, Book> {
    bookList: Book[];
    bookBorderstyle = {}
    constructor(props) {
        super(props);
        this.state = new Book();
        //get all the books from the library.
        this.props.bookStore.getAllBooks();
    }
    toggleBookDetail = () => {
        document.getElementById("bookDetailModel").click();
    }
    render() {
        let filteredBooks = this.props.bookStore.filterBookList;
        return (
            <Fragment>
                {/* book List area */}
                <div className="container">
                    <div className="row">
                        {
                            // if we remove the condition "book.availableCount>0" the book will not be removed from the library Instead of that it will just show a disabled effect to acknowledge the user that the book is not available anymore. 
                            filteredBooks.length > 0 ? (
                                this.props.bookStore.filterBookList.map((book, idx) => (
                                    book.availableCount > 0 ?
                                        <BookComponent
                                            params={book}
                                            bookStore={this.props.bookStore}
                                            key={idx}
                                            showDetails={(book: Book) => { this.setState(book, () => { this.toggleBookDetail() }); }} /> : ""
                                ))) : (<NoBook />)
                        }
                    </div>
                </div>

                {/* selected book details area */}
                <button type="button" style={{ display: "none" }} className="btn btn-info btn-lg" data-toggle="modal" id="bookDetailModel" data-target="#myModal">Open Modal</button>
                <div id="myModal" className="modal fade bd-example-modal-lg" role="dialog">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div className="modal-body">
                                <div className="row">
                                    <div className="col-sm-6">
                                        <p>Title: {this.state.title}</p>
                                        <p>Authors: {...this.state.authors}</p>
                                        <p>Page count : {this.state.pageCount}</p>
                                        <p>Published date : {this.state.publishedDate ? new Date(this.state.publishedDate).toLocaleDateString() : ""}</p>
                                        <p>categories : {...this.state.categories}</p>
                                        <p>isbn : {this.state.isbn}</p>
                                    </div>
                                    <div className="col-sm-6 text-right">
                                        <img className="image3d" src={this.state.thumbnailUrl ? this.state.thumbnailUrl : AppEnum.defaultThumb} alt="sans"  width="200px" />
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" className="btn btn-warning pull-right" onClick={() => { this.props.bookStore.addBookToCart(this.state); this.toggleBookDetail() }}>Borrow</button>
                            </div>
                        </div>

                    </div>
                </div>

            </Fragment>
        )
    }
}

