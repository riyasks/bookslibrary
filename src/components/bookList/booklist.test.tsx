import React from "react";
import { shallow, ShallowWrapper } from "enzyme";
import { BooksList } from "./books.list";
import { BookStore } from "../../store/books.store";
import { App } from "../../app";

var appWrapper: ShallowWrapper<any, Readonly<{}>, React.Component<{}, {}, any>>
var bookList: ShallowWrapper<any, Readonly<{}>, React.Component<{}, {}, any>>
var bookstore;
describe("Books List", () => {
    beforeAll(() => {
        appWrapper = shallow(<App />)
        bookList = appWrapper.find(BooksList);
        bookstore = new BookStore
    });
    it("books list component contains atleast one element", () => {
        expect(bookList).toHaveLength(1);
    });
    it("The books list of array inside the store should be greater than zero", () => {
        expect(bookstore.bookList.length).toBeGreaterThan(0);
    });
    it("The books list should contain a property _id", () => {
        expect(bookstore.bookList[0]).toHaveProperty("_id")
    });
    it("The state should be defined in the book list", () => {
        expect(bookList).not.toBeNull();
    });
});


