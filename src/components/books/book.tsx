import * as React from "react"
import { Book } from "../../models/book";
import { AppEnum } from "../../enums/app.enums";
import { BookStore } from "../../store/books.store";



interface BookModel {
    params: Book,
    bookStore: BookStore,
    showDetails(_book: Book)
}


const BookComponent: React.SFC<BookModel> = (props) => {
    return (
        <React.Fragment>
            {/* Book section */}
            <div className="col-lg-3 col-md-4 col-sm-12 showHand">
                <div title={
                    
                    props.params.availableCount === 0 ?
                        AppEnum.bookNotAvailable : props.params.longDescription == "" ?
                            AppEnum.noDescriptionFound : props.params.longDescription}
                    className={props.params.availableCount === 0 ? "card booknotavailable" : "card"}>
                    <img onClick={() => {
                        props.showDetails(props.params)
                    }} style={{ width: "100%", height: "300px" }} src={props.params?.thumbnailUrl == "" ? AppEnum.defaultThumb : props.params.thumbnailUrl} alt="" />
                    <div className="card-block cardblockshadow">
                        <p className="card-title cut-text" >{props.params.title}</p>
                        <p className="card-text" onClick={() => {
                        props.showDetails(props.params)
                    }}>Available : {props.params?.availableCount}</p>
                        <button type="button" className="btn btn-warning" style={{ pointerEvents: props.params.availableCount === 0 ? "none" : "all",width:"inherit" }} onClick={() => { props.bookStore.addBookToCart(props.params); }}>Borrow</button>
                    </div>
                </div>
            </div>
        </React.Fragment>

    )
}

export default BookComponent