import React from "react";
import { mount, ReactWrapper } from "enzyme";
import BookComponent from "../books/book";
import { Book } from "../../models/book";
import { BookStore } from "../../store/books.store";

var bookComponent: ReactWrapper<any, Readonly<{}>, React.Component<{}, {}, any>>
var bookstore;

describe("Books", () => {
    beforeAll(() => {
        var book = new Book;
        bookstore = new BookStore
        bookComponent = mount(<BookComponent bookStore={bookstore} params={book} showDetails={() => { }} />)

    });
    it("books component contains atleast one element", () => {
        expect(bookComponent).toHaveLength(1);
    });
    it("books component contains specific css classes", () => {
        //These are the css classed needed for the Book component card
        expect(bookComponent.find(".showHand").hasClass("col-lg-3 col-md-4 col-sm-12 showHand")).toBe(true)
        expect(bookComponent.find(".card-block").hasClass("cardblockshadow")).toBe(true)
        expect(bookComponent.find(".card-title").hasClass("cut-text")).toBe(true)
    });

});
