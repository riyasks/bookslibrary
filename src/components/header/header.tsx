import * as React from "react"
import { inject, observer } from "mobx-react";
import { AppEnum } from "../../enums/app.enums";
import BorrowedListComponent from "../borrowedList/borrowed.list";
import { Fragment } from "react";
import { BookProps } from "../../models/props";

const AppHeader: React.SFC<BookProps> = (props) => {

    let cart = props.bookStore.userCart
    const filterList = event => {
        let books = props.bookStore.bookList;
        let value = event.target.value.toLowerCase();
        let filterBooks = books.filter((book) => { return book.title.toLowerCase().includes(value) });
        props.bookStore.setFilteredBooks(value == "" ? props.bookStore.bookList : filterBooks);
    }
    const toggleBorrowList = () => {
        var display = document.getElementById("BorrowedBooks").style.display;
        document.getElementById("BorrowedBooks").style.display = display === "inherit" ? "none" : "inherit";
    }
    return (

        <Fragment>
            <nav className="navbar navbar-expand-md navbar-dark bg-success bg-dark sticky-top">
                <a className="navbar-brand" id="heading">{AppEnum.appHeading}</a>
                <div className="col-sm-3 col-md-3 pull-right">
                    <form className="navbar-form" role="search">
                        <div className="input-group">
                            <input type="text" className="form-control" onChange={(e) => { filterList(e) }} placeholder="Search book title" name="srch-term" id="srch-term" />
                            <div className="input-group-btn">
                                <button className="btn btn-default" type="submit"><i className="glyphicon glyphicon-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="navbarCollapse" className="collapse navbar-collapse">
                    <ul className="nav navbar-nav ml-auto">
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle showHand" onClick={() => { toggleBorrowList() }}>
                                <mark className="badge">{cart.length === 0 ? "" : cart.length}</mark>
                                {AppEnum.cartName}
                            </a>
                            <div className="dropdown-menu dropdown-menu-right" id="BorrowedBooks" style={{ minWidth: "26rem", padding: "0px 0px 0px" }}>
                                {<BorrowedListComponent bookStore={props.bookStore} />}
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </Fragment>
    )
}

export default inject('bookStore')(observer(AppHeader))