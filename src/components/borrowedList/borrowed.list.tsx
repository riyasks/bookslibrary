import * as React from "react"
import { AppEnum } from "../../enums/app.enums";
import { BookProps } from "../../models/props";

const BorrowedListComponent: React.SFC<BookProps> = (props) => {
    let cart = props.bookStore.userCart;
    return (
        <React.Fragment>
            {
                cart.length > 0 ? (
                    <table className="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Title</th>
                                <th>Isbn</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                cart.map((book, idx) => (
                                    <tr key={idx}>
                                        <td>
                                            <img src={book.thumbnailUrl ? book.thumbnailUrl : AppEnum.defaultThumb} className="img-responsive img-thumbnail" alt="Cinque Terre" />
                                        </td>
                                        <td>{book.title}</td>
                                        <td>{book.isbn}</td>
                                        <td><button type="button" onClick={() => {
                                            props.bookStore.removeBookFromCart(book);
                                        }} className="btn btn-warning btn-circle"><i className="fa fa-times"></i>
                                        </button>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                ) : (
                        <div className="nodata">
                            {AppEnum.noBooksFound}
                        </div>
                    )
            }
        </React.Fragment>
    )
}
export default BorrowedListComponent