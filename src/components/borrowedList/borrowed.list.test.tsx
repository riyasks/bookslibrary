import React from "react";
import { mount, ReactWrapper } from "enzyme";
import BorrowedListComponent from "../borrowedList/borrowed.list";
import { BookStore } from "../../store/books.store";

var _borrowedListComponent: ReactWrapper<any, Readonly<{}>, React.Component<{}, {}, any>>
var bookstore;

describe("Borrowed List", () => {
    beforeAll(() => {
        bookstore = new BookStore
        _borrowedListComponent = mount(<BorrowedListComponent bookStore={bookstore} />)

    });
    it("borrowed list component contains atleast one element", () => {
        expect(_borrowedListComponent).toHaveLength(1);
    });
    it("borrowed list component contains specific css classes and emty message", () => {
        expect(_borrowedListComponent.find(".nodata").text()).toBe("Sorry no books found!")
    });

});
