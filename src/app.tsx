import React from 'react'
import { Component } from 'react'
import { Provider } from 'mobx-react'
import { BooksList } from './components/bookList/books.list'
import { BookStore } from './store/books.store'
import AppHeader from './components/header/header'


export class App extends Component {
  //Inject the bookstore 
  private bookStore: BookStore = new BookStore()
  render() {
    return (
      <Provider bookStore={this.bookStore}>
        <AppHeader bookStore={this.bookStore} />
        <BooksList />
      </Provider>
    )
  }
}
