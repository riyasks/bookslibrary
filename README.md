# Books Library Web Application With React, TypeScript MobX,Bootrap 4 and Webpack 4

This is the repository for books library web application used to borrow  books from the library.

This project is written in typescript language to make easy to implement the requirements in object oriented manner. 

This repository does not include any back end application due to this multiple user scenario is not considered.

The data is in the format of json and it is included in the project.

**Implemented the git pipeline for build and test automatically after commiting in the repository**

**Included the CI/CD gitlab for building and unit testing**


Library App requirements

**Stories**

- User can view all the books in the library
- The user can view the description of the book on hovering the book.
- The user can choose the desired books from the list and add it into the borrowed list in the header.
- "No books found!" message will be shown when the user search for a particular book which is not available in the library.
- When the library is empty, it will show the same message as above. currently we added 300 books to the json file.
- If the user add a book into the borrowed list that particular book will be removed from the library.
- Each user can have a borrowing limit of 2 books at any point of time.
- The library may contain more than one copy of the same book.
- Only one copy of the same book can be borrowed by a user at any point of time.
- The user can remove the selected books from the borrowed list and it will be added to the library.
- When the user removes the books from the borrowed list the number of available books will be updated simultaneously in the book list as well as in the borrowed list

**Notes**

*if we remove the condition `book.availableCount>0` from the file book.list.tsx component the book will not be removed from the library Instead of that it will just show a disabled effect to acknowledge the user that the book is not available now. *




To install:

    $ npm install

To run:

    $ npm start

To build:

    $ npm run build

To test:

    $ npm test

To configure the enzyme and jest for unit testing used the following article
https://medium.com/@mateuszsokola/configuring-react-16-jest-enzyme-typescript-7122e1a1e6e8
